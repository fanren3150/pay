<?php
namespace fanren\pay\lib\cspay;
class CsPay {
    private $config = [];
    public $error = '';
    /**
     * @param array $config <ul>
     * <li>string $key AppKey </li>
     * <li>string $secret AppSecret </li>
     * <li>string $sign 短信签名 </li>
     * </ul>
     */
    public function __construct(array $config) {
        $this->config = $config;
    }
    public function start() {
        
    }
    
    public static function getMillisecond() {
        list ( $s1, $s2 ) = explode(' ', microtime());
        return (float) sprintf('%.0f', (floatval($s1) + floatval($s2)) * 1000);
    }
}
