<?php
namespace fanren\pay;
class Pay {
    private $pay = null;
    public function __construct($config, $type = 'CsPay') {
        if (file_exists(__DIR__ . "/lib/{$type}.php")) {
            include(__DIR__ . "/lib/{$type}.php");
            $this->sms = new $type($config);
        }else{
            throw new \Exception('no pay type');
        }
    }
    
    /**
     * 发送普通短信
     * @param string|array $mobile
     * @param array $params
     * @return boolean
     */
    public function sendSms($mobile, $params = []) {
        return $this->pay->sendSms($mobile, $params);
    }
    
    public function getError() {
        return $this->pay->error;
    }
}